#!/bin/bash

cd $(dirname $0);__DIR__=$(pwd)

FROM2ALL_DIR=/dados/sites/collaborative/app/from2all

# Clear old instalations
rm docker-compose.yml
rm -R ${FROM2ALL_DIR}/files

echo "version: \"3\"
services:
    app:
        container_name: from2all
        image: adsdatacloud/from2all:latest
        ports:
            - \"14081:80\"
        volumes:
            - ${FROM2ALL_DIR}/files:/files
            - ${FROM2ALL_DIR}/licence:/licence
        restart: always " \
    > docker-compose.yml

sudo docker-compose pull
sudo docker-compose down --volumes && sudo docker-compose up -d --build

mkdir -p ${FROM2ALL_DIR}/files/out/pdf
mkdir -p ${FROM2ALL_DIR}/files/out/mp4
sudo chmod 0777 -R ${FROM2ALL_DIR}