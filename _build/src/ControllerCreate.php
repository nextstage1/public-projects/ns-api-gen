<?php

class ControllerCreate {

    public static $template;

    public static function save($dados, $pathToSave) {
        // Os controllers do framework estarão debaixo de uma pasta pois raramente receberão atenção
        if (substr($dados['tabela'], 0, 3) === 'app') {
            $pathToSave .= DIRECTORY_SEPARATOR . 'library';
        }

        // Entidades sistemicas que somente estendem o padrão
        if (array_search($dados['entidade'], SistemaCreate::$entitesFrameworkToIgnore) === false) {
            $template = self::get($dados);
        } else {
            $template = self::getExtendDefault($dados['entidade']);
        }

        // controle dinamico de overwrite
        $file = $pathToSave . DIRECTORY_SEPARATOR . $dados['entidade'] . 'Controller.php';
        if (file_exists($file)) {
            $hashContentFile = hash('sha256', file_get_contents($file));
            $hashContent = hash('sha256', $template);
            $overwrite = \NsUtil\Helper::compareString($hashContent, $hashContentFile);
        }
        $sobrepor = (($overwrite) ? 'SOBREPOR' : 'w+');

        // salvar arquivo
        NsUtil\Helper::saveFile($file, false, $template, $sobrepor);
        return true;
    }

    private static function get($dados) {
        self::$template = file_get_contents(__DIR__ . '/templates/controller.php');
        $dados['date'] = date('d/m/Y');
        $dados['datetime'] = date('c');
        $condicoes = [];
        foreach ($dados['atributos'] as $atributo) {
            if (strtolower(substr($atributo['nome'], 0, 2)) === "id") {
                $tabelaRelacional = ucwords(substr($atributo['nome'], 2, 150));
                $condicoes[] = "'id$tabelaRelacional'";
            }
        }
        $dados['condicoes'] = ' // IDs esperados
                foreach ([' . implode(",", $condicoes) . '] as $v) {
            if ((int) $dados[$v] > 0) {
                $this->condicao[$v] = (int) $dados[$v];
            }
        }';

//        var_export($dados); die();
        // json config
        $jsonConfig = [];
        foreach ($dados[arrayCamposJson] as $item) {
            $jsonConfig[] = "$item => \n'nome_variavel' =>[\n['default' => '', 'grid' => 'col-sm-4', 'type' => 'text', 'class' => '', 'ro' => 'false', 'tip' => '', 'label' => '']\n],";
        }
        $dados['jsonConfig'] = implode("\n", $jsonConfig);

        $dados['namespace'] = \MyApp\Library\Config::getData('psr4Name');
        $dados['schemaEntitie'] = (($dados['schema'] === 'public') ? '' : ucwords(NsUtil\Helper::name2CamelCase($dados['schema'])) . '\\');

        $out = (new \NsUtil\Template(self::$template, $dados, '%', '%'))->render();
        return $out;
    }

    private static function getExtendDefault($entidade) {
        return '<?php
/** Created by NsLibrary Framework **/
if (!defined("SISTEMA_LIBRARY")) {die("Direct access not allowed. Define the SISTEMA_LIBRARY contant to use this class.");}               


/**
* @date ' . date('c') . '
*/

class ' . $entidade . 'Controller extends ' . $entidade . 'ControllerLibrary {
    public function __construct() {
        parent::__construct();
    }
}';
    }
}
