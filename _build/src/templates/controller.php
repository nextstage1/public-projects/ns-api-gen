<?php

namespace %namespace%\Controllers;

use NsLibrary\Config;
use NsLibrary\Controller\ApiRest\AbstractApiRestController;
use NsUtil\Api;
use MyApp\Library\Poderes;

/** Created by NsLibrary Framework * */
if (!defined("SISTEMA_LIBRARY")) {
    die("Direct access not allowed. Define the SISTEMA_LIBRARY contant to use this class.");
}



class %entidade%Controller extends AbstractApiRestController {

    public function __construct(Api $api) {
        $this->init($api);
        $entidadeName = '%entidade%';
        $entidadeObject = new \%namespace%\NsLibrary\Entities\%schemaEntitie%%entidade%();
        $poderesGrupo = '%entidade%';
        $poderesSubGrupo = '%entidade%';
        $camposDate = [%camposDate%];
        $camposDouble = [%camposDouble%];
        $camposJson = [%camposJson%];
        $camposCrypto = \NsLibrary\Config::getData('camposCrypto')[$entidadeName] ?? [];
        $this->controllerInit($entidadeName, $entidadeObject, $poderesGrupo, $poderesSubGrupo, $camposDate, $camposDouble, $camposJson, $camposCrypto);
    }
    
    
//    public function ws_getNew() {
//        Poderes::verify($this->poderesGrupo, $this->poderesSubGrupo, 'Inserir');
//        return parent::ws_getNew();
//    }

    public function read() {
        Poderes::verify($this->poderesGrupo, $this->poderesSubGrupo, 'Ler');
        $out = $this->ws_getById($this->dados);
        $this->response($out, Api::HTTP_OK);
    }

    public function list() {
        Poderes::verify($this->poderesGrupo, $this->poderesSubGrupo, 'Ler');
        $out = $this->ws_getAll($this->dados);
        $this->response($out, Api::HTTP_OK);
    }
    
    public function create() {
        Poderes::verify($this->poderesGrupo, $this->poderesSubGrupo, 'Inserir');
        unset($this->dados['id']);
        $out = $this->ws_save($this->dados);
        $this->response($out, Api::HTTP_OK);
    }
    
    public function update() {
        $this->dados['id' . $this->ent] = $this->dados['id'];
        Poderes::verify($this->poderesGrupo, $this->poderesSubGrupo, 'Editar');
        $out = $this->ws_save($this->dados);
        $this->response($out, Api::HTTP_OK);
    }

    /**
     * Metodo responsavel por remover uma entidade
     */
    public function delete() {
        Poderes::verify($this->poderesGrupo, $this->poderesSubGrupo, 'Remover');
        $out = $this->ws_remove($this->dados);
        $this->response($out, Api::HTTP_OK);
    }
    
}
