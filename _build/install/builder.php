<?php

use NsUtil\Deployer\DeployerGenerator;
use NsUtil\Helper;
use NsUtil\Package;

ini_set('display_errors', 1);
error_reporting(E_ERROR);

$pathAplicacao = realpath(__DIR__ . '/../../');
$pathIoncubeProject = realpath(__DIR__ . '\ioncube') . DIRECTORY_SEPARATOR . 'ioncube-post.bat';
$pathOutput = realpath(__DIR__ . '\..\Application');

require $pathAplicacao . '/vendor/autoload.php';

// Obtendo os valores de .env
$inifile = Helper::fileSearchRecursive('.env', __DIR__);
$_CONFIG = [];
if (file_exists($inifile)) {
    $_CONFIG = \parse_ini_file($inifile, true);
}


Helper::mkdir($pathOutput);

// pastas e diretorios especificos desta aplicacao, contando sempre  da raiz, nao recursivos
$excluded = [
    // 'vendor/*',
    'sch.php',
    'ingest/',
    'storage/',
    'app/',
    'test/',
    'cmd*',
    '*.env*',
    'git.php',
    'README.md',
    'Dockerfile',
    'sample/',
    'docker-*'
];

// Gerando build
shell_exec('cls');
// Package::setUrlLocalApplication('http://localhost:9000');
Package::setProjectName($_CONFIG['ns_api_modelo']);
$projectName = Package::run($pathAplicacao, $excluded, $pathOutput, $pathIoncubeProject);

// Acionar ioncube
// echo "\nCodificando arquivos PHP";
// $cmd = 'call ' . __DIR__ . '\ioncube\encoder-to-production.bat';
// shell_exec($cmd);
// Gerando build
echo "\nGerando build";
$cmd = 'call ' . $pathIoncubeProject . ' > nul';
shell_exec($cmd);
shell_exec('del ' . $pathOutput . DIRECTORY_SEPARATOR . $projectName . '-package.zip');

// Gerar deploy files
echo "\nGerando deploy files";
$pathToKeySSH = '';
$ownerOnServer = '';
$pathOnServer = '';
$userDeployer = '';
$host = '';
$sudoRequire = true;
$installCrontab = false;
(new DeployerGenerator($projectName . '-encoded.zip', $pathOutput))
        ->addConfig($projectName . '_main', $pathOnServer . '/prod', $ownerOnServer, $pathToKeySSH, $userDeployer, $host, $installCrontab, $sudoRequire)
        ->run(__DIR__, '7.4');
