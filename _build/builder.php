<?php
die();

use MyApp\Library\App;
use MyApp\Library\Config;
use NsLibrary\Builder\Create;
use NsUtil\Eficiencia;

require __DIR__ . '/../vendor/autoload.php';

// sempre local
App::init();
App::setDevelopeMode();

// Eficiencia
$ef = new Eficiencia();

// Builder
$schemas = explode(',', Config::getData('SCHEMAS'));
$builder = new Create($schemas);
$data = $builder->getData(true);
echo $builder->run();
echo "Construção de entidades: " . $ef->end()->text . PHP_EOL;

// Controllers
require __DIR__ . '/src/ControllerCreate.php';
require __DIR__ . '/src/SistemaCreate.php';
$pathToSaveControllers = __DIR__ . '/../src/Controllers';

// Lista de tabelas a ignorar nesta aplicação
$ignore = explode(',', Config::getData('IGNORETABLES')) ?? [];

// Criação dos padrões
foreach ($data['itens'] as $dados) {
    if (in_array($dados['table'], $ignore) === false) {
        ControllerCreate::save($dados, $pathToSaveControllers);
    }
}