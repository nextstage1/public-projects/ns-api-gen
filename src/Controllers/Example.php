<?php

namespace MyApp\Controllers;

use NsLibrary\Controller\ApiRest\AbstractApiRestController;
use NsUtil\Api;

class Example extends AbstractApiRestController {

    public function __construct(Api $api) {
        $this->init($api);
    }

    public function list(): void {
        $this->response(['TEST' => 'OK!']);
    }

}
