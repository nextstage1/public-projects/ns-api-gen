<?php

namespace MyApp\Library;

use Exception;
use NsLibrary\SistemaLibrary;
use NsUtil\ConnectionPostgreSQL;
use NsUtil\ErrorHandler;
use NsUtil\Helper;

class App {

    private static $con;

    /**
     * Inicia as configurações da aplicação e itens de segurança
     */
    public static function init($nivelLogsPHP = 1) {
        \Config::init();
        SistemaLibrary::initByConfig(Config::getAll());

        date_default_timezone_set('UTC');

//        // Setar itens da tabela Config para a variavel config
//        $con = Connection::getConnection();
//        $config = $con->execQueryAndReturn("SELECT extras_app FROM app WHERE id_app= 1")[0];
//        if (!is_array($config)) {
//            $this->logTracker('Configuração do loadbalancer (table config) não esta definida', [], true);
//            die('Identificado um erro. Developers foram notificados. (ERR-28)');
//        }
//        Config::addFromArray($config);

        SistemaLibrary::setSecurity(0);
        
        $err = new ErrorHandler('/tmp/conversorlb_php_error.csv', 'LoadBalancerConversor', true);
    }

    public static function setDevelopeMode() {
        error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
    }

    /**
     * Retorna uma conexão
     * É necessário existir a configuração de Config para tal
     * @return ConnectionPostgreSQL
     */
    public static function getConnection() {
        //new \NsUtil\Connection\SQLite(__DIR__ . '/../storage/db.sqlite');

        if (!self::$con) {
            try {
                $config = Config::getData('database');
                self::$con = new ConnectionPostgreSQL($config['host'], $config['user'], $config['pass'], $config['port'], $config['dbname']);
            } catch (Exception $exc) {
                echo $exc->getMessage();
            }
        }
        return self::$con;
    }

    public static function validaApiKey($apikey) {
        $api = new ApiUsers();
        $list = $api->list(['apikeyApiUsers' => $apikey]);
        return $list[0] instanceof ApiUsers;
    }

    /**
     * Regitra um log no sistema LogTracker
     * @param string $text
     * @param array $extras
     * @param bool $notifyDevops
     */
    public static function logTracker(string $text, array $extras = [], bool $notifyDevops = false) {
        try {
            // Logar local tbem
            $log = new \JDrel\NsLibrary\Entities\Log();
            $log->setJsonLog(array_merge($extras, ['text' => $text]));
            $log->setTypeLog('ERROR');
            $log->save();
        } catch (Exception $exc) {
            
        }

        $url = Config::getData('logtrackerUrl') . '/add';
        $extras['backtrace'] = debug_backtrace();
        $params = [
            'appname' => Config::getData('nameApp'),
            'text' => $text,
            'plus' => $extras,
            'notifyDevops' => $notifyDevops
        ];
        $method = 'POST';
        $header = ['Content-Type:application/json', 'apikey:' . Config::getData('logtrackerApikey')];
        $ssl = true;
        try {
            Helper::curlCall($url, $params, $method, $header, $ssl);
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

}
