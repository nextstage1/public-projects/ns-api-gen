<?php

namespace MyApp\Library;

use NsUtil\Config as Config2;
use NsUtil\Helper;

class Config {

    private static $cfg;

    private function __construct() {
        
    }

    public static function init(array $SistemaConfig = []) {
        $inifile = Helper::fileSearchRecursive('.env', __DIR__);
        $_CONFIG = [];
        if (file_exists($inifile)) {
            $_CONFIG = \parse_ini_file($inifile, true);
        }

        // Incluir a var $_config
        $envs = getenv();

        // Padrão
        $_CONFIG['psr4Name'] = Helper::getPsr4Name();
        $_CONFIG['path'] = Helper::getPathApp();
        $_CONFIG['pathTmp'] = Helper::getSO();
        $_CONFIG['GCLOUDDESATIVADO'] = $envs['GCLOUDDESATIVADO'] ?? false;

        $_CONFIG['database'] = [
            'host' => $envs['DBHOST'] ?? $_CONFIG['DBHOST'],
            'user' => $envs['DBUSER'] ?? $_CONFIG['DBUSER'],
            'pass' => $envs['DBPASS'] ?? $_CONFIG['DBPASS'],
            'dbname' => $envs['DBNAME'] ?? $_CONFIG['DBNAME'],
            'port' => $envs['DBPORT'] ?? $_CONFIG['DBPORT']
        ];

        // Token para cryptografia
        $_CONFIG['TOKEN_CRYPTO'] = $_CONFIG['TOKEN_CRYPTO'] ?? md5(\json_encode($_CONFIG['database']['host'] . $_CONFIG['database']['dbname'] . $_CONFIG['database']['user']));

        // Criar config
        $configuracao = array_merge($SistemaConfig, $_CONFIG, $envs);

        self::$cfg = new Config2($configuracao);
    }

    public static function getAll(): array {
        return self::$cfg->getAll();
    }

    public static function getData($key) {
        return self::$cfg->get($key);
    }

    public static function setData($key, $val) {
        return self::$cfg->set($key, $val);
    }

    public static function addFromArray(array $array) {
        foreach ($array as $key => $val) {
            self::$cfg->set($key, $val);
        }
    }

}
