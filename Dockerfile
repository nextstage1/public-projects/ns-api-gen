FROM php:7.4.27-apache-buster

ARG ENVIROMENT

VOLUME ["/var/www/html"]

# Install Dependencies
RUN apt-get update \
    && apt-get upgrade -y \
    && apt-get install -y libpq-dev curl wget zip unzip libpng-dev cron nano htop procps \
    && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
    && docker-php-ext-install pdo pdo_pgsql pgsql mysqli gd bcmath

# Install Imagick
RUN apt-get update && \
    apt-get install -y libmagickwand-dev --no-install-recommends && \
    pecl install imagick && \
    docker-php-ext-enable imagick
 
# Install ioncube
COPY ./docker-sample/ioncube_loader_lin_7.4.so /usr/local/lib/php/extensions/ioncube.so
COPY ./docker-sample/ioncube.ini /usr/local/etc/php/conf.d/00-ioncube.ini

# FPM Config
COPY ./docker-sample/www.${ENVIROMENT}.conf /usr/local/etc/php-fpm.d/www.conf

# User
RUN usermod -u 1000 www-data \
    && groupmod -g 1000 www-data \
    && chgrp -R staff /usr/local/etc/php-fpm.d/www.conf \
    && a2enmod rewrite \
    && a2enmod headers

## PHP security
COPY ./docker-sample/99-security.${ENVIROMENT}.ini /usr/local/etc/php/conf.d/99-security.ini

# OP Cache
RUN docker-php-ext-configure opcache --enable-opcache \
    && docker-php-ext-install opcache

# Config OPCache
COPY ./docker-sample/opcache.${ENVIROMENT}.ini /usr/local/etc/php/conf.d/opcache.ini

# AWS Config
RUN mkdir -p /var/www
COPY ./docker-sample/.aws /var/www/.aws
RUN chown www-data:www-data /var/www/.aws -R

# Apache config
RUN { \
		echo '<Files *.env>'; \
		echo '\t order allow,deny'; \
		echo '\t Deny from all'; \
		echo '</Files>'; \
                echo; \
		echo 'Protocols h2 h2c http/1.1'; \
                echo 'StartServers 10\nMaxClients 150\nMaxRequestsPerChild 0\n'; \
	} | tee "/etc/apache2/conf-available/my-server.conf" \
	&& a2enconf docker-php \
    && a2enconf my-server

RUN { \ 
    echo '<VirtualHost *:80>'; \
    echo 'ServerAdmin serveradmin@local.com.br'; \
    echo 'DocumentRoot /var/www/html/api'; \
    echo '<Directory /var/www/html/api>'; \
    echo 'Options -Indexes'; \
    echo 'AllowOverride All'; \
    echo 'Require all granted'; \
    echo '</Directory>'; \
    echo '</VirtualHost>'; \
    } | tee "/etc/apache2/sites-available/000-default.conf"
    
# Cleanup 
RUN docker-php-source delete \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /tmp/*

RUN mkdir /storage /licence
RUN chmod 0777 /storage -R

# To Build image, copy the files
COPY . /var/www/html
#RUN rm -R /var/www/html/_build

#COPY ./_build/Application/APPLICATION_NAME.zip /tmp/package.zip
#RUN unzip -o /tmp/package.zip -d /var/www/html > /dev/null
#RUN rm -R /tmp/package.zip


CMD ["/usr/sbin/apache2ctl","-DFOREGROUND"]