<?php

use NsUtil\Package;

if (strlen($argv[1]) === 0) {
    die("## Error: Informe a mensagem do commit: php git.php \"type/mensagem\"\n\n");
}

require 'vendor/autoload.php';

//include __DIR__ . '/_build/builder.php';

$message = $argv[1];
Package::git(__DIR__ . '/version', $message);