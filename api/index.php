<?php

use MyApp\Library\App;
use NsUtil\Api;
use NsUtil\Helper;

include __DIR__ . '/../vendor/autoload.php';

// Start de configs
App::init();
App::setDevelopeMode();

// Será lida como variavel global na aplicação
$dados = [];
$namespace = Helper::getPsr4Name() . '\\' . 'Controllers';
Api::restFull($namespace);
http_response_code(Api::HTTP_NOT_FOUND);
